#!/bin/bash

if command -v inotifywait; then 
  echo "let's go!"
else
  echo "error: this script requires 'inotifywait' from 'inotify-tools'"
  exit 1
fi

# handle ctrl+c (SIGINT)
trap handle_int INT

handle_int() {
    echo "exiting..."
    kill "$PID"
    exit
}


while true; do
    go build .

    if [ $? -eq 0 ]
    # when the build was successful
    then
        ./todomvc &

        PID=$!

        echo "PID: $PID"

        #restart upon file change
        inotifywait --exclude '.(git|data)' -r -e modify .
        kill "$PID"

    #when the build fails
    else    
        inotifywait --exclude '\.(git|data)' -r -e modify .
    fi
done
