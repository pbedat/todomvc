package main

import (
	"crypto/tls"
	"embed"
	"flag"
	"html/template"
	"io"
	"log"

	"github.com/gin-gonic/gin"
	"gitlab.com/pbedat/todomvc/routes"
	uplink "gitlab.com/pbedat/uplink/package"
	"golang.org/x/crypto/acme/autocert"
)

//go:embed templates/*
//go:embed public*
var content embed.FS

var hostname string
var session string
var email string

func init() {
	flag.StringVar(&hostname, "hostname", "", "sets the hostname used for the ssl cert")
	flag.StringVar(&session, "session", "", "the uplink session")
	flag.StringVar(&email, "email", "pbedat@pm.me", "used for ACME notifications")
	flag.Parse()
}

func main() {

	r := gin.Default()

	t, err := loadTemplate()
	if err != nil {
		panic(err)
	}
	r.SetHTMLTemplate(t)

	r.GET("/public/css/styles.css", loadStylesFromAssets)

	r.Use(routes.InjectController)

	r.GET("/", routes.Index)
	r.POST("/create", routes.Create)
	r.POST("/edit/:id", routes.Edit)
	r.POST("/clear-completed", routes.ClearCompleted)
	r.POST("/toggle-all", routes.ToggleAll)

	// run with SSL through uplink
	if hostname == "" {
		log.Fatal(r.Run())
	} else {
		m := autocert.Manager{
			Prompt:     autocert.AcceptTOS,
			HostPolicy: autocert.HostWhitelist(hostname),
			Email:      email,
		}

		l, err := uplink.GetListener(uplink.ListenerConfig{
			ServiceType: "HTTPS",
			Hostnames:   []string{hostname},
			Session:     session,
		})

		if err != nil {
			log.Fatal(err)
		}

		r.RunListener(tls.NewListener(l, m.TLSConfig()))
	}
}

func loadTemplate() (*template.Template, error) {
	t, err := template.ParseFS(content, "templates/*.html")

	return t, err
}

func loadStylesFromAssets(c *gin.Context) {
	f, err := content.Open("public/css/styles.css")

	if err != nil {
		c.String(404, "File not found")
		return
	}

	c.Header("Content-Type", "text/css")
	c.Header("Cache-Control", "public, max-age=31536000")

	io.Copy(c.Writer, f)
}
