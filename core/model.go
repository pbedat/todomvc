package core

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/google/uuid"
)

type Todo struct {
	Id   string
	Text string
	Done bool
}

type Todos []Todo

func (todos Todos) Create(text string) Todos {
	return append(todos, Todo{uuid.New().String(), text, false})
}

type TodoFilter = string

const (
	FilterAll       TodoFilter = "all"
	FilterActive    TodoFilter = "active"
	FilterCompleted TodoFilter = "completed"
)

func (todos Todos) Filter(filter TodoFilter) Todos {
	if filter == FilterAll {
		return todos
	}

	filtered := []Todo{}

	for _, todo := range todos {
		if !todo.Done && filter == FilterActive || todo.Done && filter == FilterCompleted {
			filtered = append(filtered, todo)
		}
	}

	return filtered
}

func (todos Todos) Completed() int {
	completed := 0

	for _, todo := range todos {
		if todo.Done {
			completed++
		}
	}

	return completed
}

type TodoChangeset struct {
	Text string
	Done bool
}

func (todos Todos) Update(id string, change TodoChangeset) (Todos, error) {

	i, err := todos.indexOf(id)

	if err != nil {
		return todos, err
	}

	todos[i].Text = change.Text
	todos[i].Done = change.Done

	return todos, nil
}

func (todos Todos) Remove(id string) (Todos, error) {
	i, err := todos.indexOf(id)

	if err != nil {
		return todos, err
	}

	return append(todos[0:i], todos[i+1:]...), nil
}

func (todos Todos) indexOf(id string) (int, error) {
	for i, t := range todos {
		if t.Id == id {
			return i, nil
		}
	}
	return -1, fmt.Errorf("todo with id '%s' does not exist", id)
}

func (todos Todos) Json() string {
	buffer, err := json.Marshal(todos)

	if err != nil {
		log.Println(err)
		return "[]"
	}

	return string(buffer)
}

func (todos Todos) ToggleAll(toggle bool) Todos {
	for i := range todos {
		todos[i].Done = toggle
	}

	return todos
}
