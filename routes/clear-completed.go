package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/pbedat/todomvc/core"
)

func ClearCompleted(c *gin.Context) {
	ctrl := provideCtrl(c)

	active := ctrl.todos.Filter(core.FilterActive)

	c.SetCookie("todos", string(active.Json()), 0, "", "", false, true)

	c.Redirect(http.StatusFound, ctrl.redirectIndex(c.GetHeader("Referer")))
}
