package routes

import (
	"fmt"

	"github.com/gin-gonic/gin"
)

func Index(c *gin.Context) {
	ctrl := provideCtrl(c)

	query := ctrl.query()

	filtered := ctrl.todos.Filter(query.filter)
	completed := ctrl.todos.Completed()

	etagHash := ctrl.etag(filtered, len(ctrl.todos), query.edit)
	etagHash = fmt.Sprintf("\"%s\"", etagHash)

	ifNoneMatch := c.GetHeader("If-None-Match")

	// we have to strip the "weak validation" prefix, that is added by proxy servers due to gzip
	// https://developer.mozilla.org/en-US/docs/Web/HTTP/Conditional_requests#Weak_validation
	if ifNoneMatch != "" && ifNoneMatch[0:2] == "W/" {
		ifNoneMatch = ifNoneMatch[2:]
	}

	if ifNoneMatch == etagHash {
		c.Status(304)
		return
	}

	c.Header("ETag", etagHash)

	c.HTML(
		200,
		"index.html",
		gin.H{
			"todos":     filtered,
			"filter":    query.filter,
			"total":     len(ctrl.todos),
			"completed": completed,
			"edit":      query.edit,
		},
	)
}
