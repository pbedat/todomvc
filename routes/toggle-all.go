package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func ToggleAll(c *gin.Context) {

	ctrl := provideCtrl(c)

	todos := ctrl.todos.ToggleAll(c.PostForm("toggleAll") == "true")

	c.SetCookie("todos", string(todos.Json()), 0, "", "", false, true)

	c.Redirect(
		http.StatusFound,
		ctrl.redirectIndex(c.GetHeader("Referer")),
	)
}
