package routes

import (
	"crypto/sha1"
	"encoding/json"
	"fmt"
	"log"
	"net/url"

	"github.com/gin-gonic/gin"
	"gitlab.com/pbedat/todomvc/core"
)

type todosController struct {
	todos core.Todos
	*gin.Context
}

func newController(c *gin.Context) *todosController {

	return &todosController{
		core.Todos{},
		c,
	}
}

const CTRL = "ctrl"

func InjectController(c *gin.Context) {
	todosCtrl := newController(c)

	todosCtrl.ParseCookie("todos")

	c.Set(CTRL, todosCtrl)
}

func provideCtrl(c *gin.Context) *todosController {
	return c.MustGet(CTRL).(*todosController)
}

func (ctrl *todosController) ParseCookie(name string) {
	todosJSON, err := ctrl.Cookie(name)

	todos := []core.Todo{}

	if err != nil {
		return
	}

	err = json.Unmarshal([]byte(todosJSON), &todos)

	if err != nil {
		log.Println(err)
	}

	ctrl.todos = todos
}

func (ctrl todosController) redirectIndex(indexUrl string) string {
	u, _ := url.Parse(indexUrl)
	q := u.Query()
	q.Del("edit")
	u.RawQuery = q.Encode()
	return u.String()
}

type action string

const (
	actionEdit   action = "edit"
	actionDelete action = "delete"
)

type indexAction struct {
	etag     string
	viewData map[string]interface{}
}

func (ctrl todosController) etag(todos core.Todos, total int, edit string) string {

	etag := fmt.Sprintf("%d:%s:", total, edit)

	for _, todo := range todos {
		etag += fmt.Sprintf("%v", todo)
	}

	etagHash := fmt.Sprintf("%x", sha1.Sum([]byte(etag)))

	return etagHash
}

type todosQuery struct {
	filter core.TodoFilter
	edit   string
}

func (ctrl todosController) query() todosQuery {
	filter := ctrl.Query("filter")

	if filter == "" {
		filter = "all"
	}

	return todosQuery{
		filter,
		ctrl.Query("edit"),
	}
}

func (ctrl todosController) form() core.Todo {
	return core.Todo{
		Text: ctrl.PostForm("Text"),
		Done: ctrl.PostForm("Done") == "true",
	}
}

func (ctrl todosController) idParam() string {
	return ctrl.Param("id")
}

type editFormData struct {
	action string
	text   string
	done   bool
}

func (ctrl todosController) editForm() editFormData {

	action := ctrl.PostForm("action")

	if action == "" {
		action = "edit"
	}

	return editFormData{
		action,
		ctrl.PostForm("Text"),
		ctrl.PostForm("Done") == "true",
	}
}
