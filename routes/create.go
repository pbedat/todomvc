package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func Create(c *gin.Context) {
	ctrl := provideCtrl(c)

	formData := ctrl.form()

	todos := ctrl.todos.Create(formData.Text)

	c.SetCookie("todos", todos.Json(), 0, "", "", false, true)

	c.Redirect(
		http.StatusFound,
		ctrl.redirectIndex(c.GetHeader("Referer")),
	)
}
