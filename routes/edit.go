package routes

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/pbedat/todomvc/core"
)

func Edit(c *gin.Context) {

	ctrl := provideCtrl(c)
	id := ctrl.idParam()
	form := ctrl.editForm()

	todos := ctrl.todos
	var err error

	switch form.action {
	case "delete":
		todos, err = ctrl.todos.Remove(id)

	case "edit":
		todos, err = ctrl.todos.Update(id, core.TodoChangeset{
			Text: form.text,
			Done: form.done,
		})

	default:
		c.String(400, fmt.Sprintf("unknown action '%s'", form.action))
		return
	}

	if err != nil {
		c.String(400, "%s", err)
	}

	c.SetCookie("todos", todos.Json(), 0, "", "", false, true)
	c.Redirect(
		http.StatusFound,
		ctrl.redirectIndex(c.GetHeader("Referer")),
	)

}
